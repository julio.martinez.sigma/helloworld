FROM python:3.9.1

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV dev
ENV DOCKER_CONTAINER 1

RUN pip install --upgrade pip

COPY . /code/
WORKDIR /code/

RUN pip install -r /code/requirements.txt
